# You'll need these imports in your own code
import os
import multiprocessing
import asyncio

# Next two import lines for this demo only
from random import choice, random
import time

from urulog.file_log import get_logger
# Arrays used for random selections in this demo

MESSAGES = [
    'Random message #1',
    'Random message #2',
    'Random message #3',
]

# This is the worker process top-level loop, which just logs ten events with
# random intervening delays before terminating.
# The print messages are just so you know it's doing something!


def run_worker_process(logger):
    asyncio.run(worker_process(logger))


async def worker_process(logger):
    name = multiprocessing.current_process().name
    print('Worker started: %s' % name)
    for i in range(3):
        time.sleep(random())
        level = choice(["DEBUG", "WARNING", "INFO"])
        message = choice(MESSAGES)
        logger.log(level, message)
    print('Worker finished: %s' % name)

# Here's where the demo gets orchestrated. Create the queue, create and start
# the listener, create ten workers and start them, wait for them to finish,
# then send a None to the queue to tell the listener to finish.


def main():
    path_log = os.path.join("logs", "loguru_parallel.log")
    logger = get_logger(id="TEST_ID", filepath=path_log, enqueue=True)
    workers = []
    for i in range(5):
        worker = multiprocessing.Process(target=run_worker_process,
                                         args=[logger,])
        workers.append(worker)
        worker.start()
    for w in workers:
        w.join()

if __name__ == '__main__':
    main()
