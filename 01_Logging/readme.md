# Zusammenfassung

Dieser Ordner enthält eine Sammlung an Methoden um logs zu intitialisieren. Es gibt zwei Ordner mit unterschiedliche Herangehensweisen:

* standard: Module und Methoden für dem Python Standard Logger
* urulog: Module und Methoden fur [loguru](https://loguru.readthedocs.io/en/stable/)