
from datetime import datetime  # import time
import logging

import pyodbc
# cf. https://stackoverflow.com/questions/2314307/python-logging-to-database

from .tsql_verification import LogTableVerification


class TsqlDBHandler(logging.Handler):
    """ Customized logging handler that puts logs to the database.
        pymssql required
    """

    def __init__(self, db_tbl_log, db_name, cfg_table_name, fmt_list):
        logging.Handler.__init__(self)
        self.db_tbl_log = db_tbl_log
        # Verbindung aufbauen
        self.sql_conn = pyodbc.connect((
            "DRIVER={ODBC Driver 17 for SQL Server};SERVER=localhost;"
            f"DATABASE={db_name};UID=sa;PWD=Rainbow07!"
        ))
        sql_cursor = self.sql_conn.cursor()
        self.fmt_list = fmt_list

        # Verify Table exists and that contents match
        verification_cls = LogTableVerification(db_name)
        table_exists = verification_cls.tsql_table_exists(cfg_table_name)
        if not table_exists:
            verification_cls.create_config_table(sql_cursor)
            self.sql_conn.commit()
        # Get column names
        self.column_list = self.__get_column_list(cfg_table_name)

        table_exists = verification_cls.tsql_table_exists(db_tbl_log)
        if not table_exists:
            verification_cls.create_log_table(sql_cursor, fmt_list, db_tbl_log)
            self.sql_conn.commit()
        verification_cls.tsql_verify_table(db_tbl_log, fmt_list)
        # setting the sql query
        self.insert_query = self.__set_sql_query()

    def __get_column_list(self, cfg_table_name: str) -> list:
        """ Create a list of columns
        """
        sql_cursor = self.sql_conn.cursor()
        fmt_tuple = tuple(item for item in self.fmt_list)
        # Holen der Spaltennamen
        sql_query = f"SELECT LogTableField FROM {cfg_table_name}"
        sql_query += f" WHERE LogAttribute IN {str(fmt_tuple)}"
        sql_cursor.execute(sql_query)
        column_list = []
        for row in sql_cursor.fetchall():
            for column in row:
                column_list.append(column)
        return column_list

    def __set_sql_query(self):
        """ Sets the sql Query for insertion
        """
        # Make the SQL insert
        sql_query = f"INSERT INTO {self.db_tbl_log} \n"
        sql_query += "(\n"
        values_part = ""
        for column_name in self.column_list:
            sql_query += column_name
            sql_query += ",\n"
            values_part += "?,"
        sql_query = sql_query.rsplit(",\n", maxsplit=1)[0]
        sql_query += "\n)\n"
        sql_query += "VALUES \n"
        sql_query += f"({values_part[:-1]})"
        return sql_query

    def emit(self, record: logging.LogRecord):
        """ Adds the log to the specified sink
        """
        sql_cursor = self.sql_conn.cursor()
        param_list = []
        for item in self.fmt_list:
            param_name = item.lstrip("%(").split(")", maxsplit=1)[0]
            if "asctime" in param_name:
                param = datetime.fromtimestamp(record.created)
            elif "message" in param_name:
                param = record.msg
            else:
                param = getattr(record, param_name)
            if param_name == "message":
                # Clear the log message so it can be put to db via sql (escape quotes)
                param = param.strip().replace('\'', '\'\'')
            param_list.append(param)
        try:
            sql_cursor.execute(self.insert_query, tuple(param_list))
            self.sql_conn.commit()
        # If error - print it out on screen. Since DB is not working - there's
        # no point making a log about it to the database :)
        except Exception as e:
            print(e)
