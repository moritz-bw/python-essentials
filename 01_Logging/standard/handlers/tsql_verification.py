
from dataclasses import replace
from operator import mod
import os
import pyodbc


class LogTableVerification:
    """ Creates LogTable
    """

    def __init__(self, db_name: str) -> None:
        """ 
        """
        self.db_name = db_name
        self.cfg_table = "cfg.T_PythonLogs"

    def tsql_table_exists(self, table_name: str) -> bool:
        """ Verify that the table exists
        """
        schema_name, table_name = table_name.split(".")
        sql_query = "SELECT * FROM INFORMATION_SCHEMA.TABLES "
        sql_query += f" WHERE TABLE_NAME = {repr(table_name)}"
        sql_query += f" AND TABLE_SCHEMA = {repr(schema_name)}"
        # Specifying the ODBC driver, server name, database, etc. directly
        conn = pyodbc.connect((
            "DRIVER={ODBC Driver 17 for SQL Server};SERVER=localhost;"
            f"DATABASE={self.db_name};UID=sa;PWD=Rainbow07!"
        ))
        # Create a cursor from the connection
        cursor = conn.cursor()
        # Sample select query
        cursor.execute(sql_query)
        row_list = cursor.fetchall()
        if len(row_list) == 0:
            return False
        return True

    def create_log_table(self, cursor, fmt_list: list, table_name: str):
        """ Creates the log table
        """
        sql_query = "SELECT CONCAT(LogTableField, ' ', LogTableFieldType, "
        sql_query += " ' ', LogTableFieldNullable) as column_sql \n"
        sql_query += " FROM cfg.T_PythonLogs tpl \n"
        sql_query += f" WHERE LogAttribute IN {str(tuple(fmt_list))}"
        cursor.execute(sql_query)
        create_query = f"CREATE TABLE {table_name}\n"
        create_query += "(\n"
        create_query += "LogEntryID INT PRIMARY KEY CLUSTERED IDENTITY(1,1),\n"
        for row in cursor.fetchall():
            for value in row:
                create_query += value
                create_query += ",\n"
        # clean up of the create query
        create_query = create_query.rsplit(",\n", maxsplit=1)[0]
        create_query += "\n)\n"
        cursor.execute(create_query)

    def create_config_table(self, cursor):
        """ Creates the config table
        """
        cfg_schema, _ = self.cfg_table.split(".")
        # CREATE SCHEMA if not already exists
        sql_query = f"""IF NOT EXISTS ( SELECT *
            FROM    sys.schemas
            WHERE   name = N'{cfg_schema}' )
            EXEC('CREATE SCHEMA [{cfg_schema}]');
            """
        cursor.execute(sql_query)
        path_create_script = os.path.join(
            "standard", "log_cfg_table.sql")
        with open(path_create_script, mode="r", encoding="utf-8") as file:
            sql_query = file.read().replace(
                "config_table", self.cfg_table)
            for subquery in sql_query.split("\n\n"):
                cursor.execute(subquery)

    def tsql_verify_table(self, table_name: str, fmt_elem_list: list) -> bool:
        """ Verifies the desired columns exist
        """
        schema_name, table_name = table_name.split(".")
        path_compare_script = os.path.join(
            "standard", "compare_field_types.sql")
        with open(path_compare_script, mode="r", encoding="utf-8") as file:
            sql_query = file.read().replace("log_table_name", table_name)\
                .replace("config_table", self.cfg_table)\
                .replace("DB_Name", self.db_name)\
                .replace("log_schema", schema_name)
        # print(sql_query)

        # Specifying the ODBC driver, server name, database, etc. directly
        conn = pyodbc.connect((
            "DRIVER={ODBC Driver 17 for SQL Server};SERVER=localhost;"
            f"DATABASE={self.db_name};UID=sa;PWD=Rainbow07!"
        ))
        # Create a cursor from the connection
        cursor = conn.cursor()
        # Sample select query
        cursor.execute(sql_query)
        row_list = cursor.fetchall()
        # If the amount of columns is not compatible
        if len(row_list) != len(fmt_elem_list):
            raise RuntimeError(
                f"{table_name} is incompatible with {fmt_elem_list}")

    def get_column_list(self, db_name: str, cfg_table_name: str, fmt_list: list):
        """ Create a list of columns
        """
        # Verbindung aufbauen
        conn = pyodbc.connect((
            "DRIVER={ODBC Driver 17 for SQL Server};SERVER=localhost;"
            f"DATABASE={db_name};UID=sa;PWD=Rainbow07!"
        ))
        cursor = conn.cursor()
        fmt_tuple = tuple(item for item in fmt_list)
        # Holen der Spaltennamen
        sql_query = f"SELECT LogTableField FROM {cfg_table_name}"
        sql_query += f" WHERE LogAttribute IN {str(fmt_tuple)}"
        cursor.execute(sql_query)
        column_list = []
        for row in cursor.fetchall():
            for column in row:
                column_list.append(column)
        return column_list


if __name__ == "__main__":
    table_name = "dbo.T_LogEntries"
    db_name = "TestDB"
    fmt_list = [
        "%(asctime)s", "%(levelname)-8s", "%(message)s"
    ]
    cls_obj = LogTableVerification(db_name)
    cls_obj.tsql_verify_table(table_name, fmt_list)
    test = cls_obj.get_column_list(db_name, cls_obj.cfg_table, fmt_list)
    print("done")
