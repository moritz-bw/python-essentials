CREATE TABLE config_table
(
	LogAttribute VARCHAR(25) NOT NULL,
	LogTableField VARCHAR(25) NOT NULL,
	LogTableFieldType VARCHAR(20) NOT NULL,
	LogTableFieldNullable VARCHAR(8) NOT NULL
);

INSERT INTO config_table
(
    LogAttribute,
    LogTableField,
    LogTableFieldType,
    LogTableFieldNullable
)
VALUES
('%(asctime)s', 'LogTime', 'DATETIME2','NOT NULL'),
('%(levelname)-8s', 'LogLevel', 'VARCHAR(8)' , 'NOT NULL'),
('%(message)s', 'LogMessage', 'NVARCHAR(255)', 'NOT NULL'),
('%(name)s', 'LogName', 'VARCHAR(100)', 'NOT NULL'),
('%(lineno)d', 'LineNumber', 'INT', 'NOT NULL'),
('%(funcName)s', 'FunctionName', 'VARCHAR(50)', 'NOT NULL'),
('%(module)s', 'ModuleName', 'VARCHAR(50)','NOT NULL'),
('%(process)d', 'ProcessID', 'INT', 'NOT NULL'),
('%(processName)-10s', 'ProcessName', 'VARCHAR(11)', 'NOT NULL');
