
import os
import logging
from handlers.mssql import TsqlDBHandler


class InitLogging:
    def __init__(self, name=None, detail=False) -> None:
        """ Sets Logging attributes
        Args:
            name: str. Logname. Default sets root-Logger
            detail: bool. Default `False` sets time, level, msg.
            Setting to `True` adds name, lineno, funcName, module, processName
        """
        self.format_string = self.create_format_string(
            format_sep="||", detail=detail)
        self.name = name
        # Wert ist der aktuelle Skriptname
        foldername, py_filename = __file__.rsplit(os.path.sep, maxsplit=2)[1:]
        self.filename = py_filename.rsplit(".", maxsplit=1)[0]
        self.logpath = os.path.join(
            "logs", f"{foldername}_{self.filename}.log")
        self.start_msg = "New Logger-Class initialised"

    def create_format_string(self, format_sep: str, detail=False) -> str:
        """ Erstellt den Log-Format String
        Args:
            format_sep: str. Trenner fuer den Format-String
            detail: bool. Default `False` sets time, level, msg.
            Setting to `True` adds name, lineno, funcName, module, processName
        """
        basic_format_items = [
            "%(asctime)s", "%(levelname)-8s", "%(message)s"
        ]
        if detail:
            basic_format_items.append("%(name)s")
            basic_format_items.append("%(lineno)d")
            basic_format_items.append("%(funcName)s")
            basic_format_items.append("%(module)s")
            # basic_format_items.append("%(process)d") works also in non-parallel
            basic_format_items.append("%(processName)-10s")
        fmt_string = f" {format_sep} ".join(basic_format_items)
        return fmt_string

    def create_file_log_class(self, level: str) -> logging.Logger:
        """ Erstellt Logger-Klasse
        Args:
            level: str. Loglevel z.B. DEBUG, INFO etc.
        """
        log_cls = logging.getLogger()
        if self.name:
            log_cls = logging.getLogger(name=self.name)
        fmt_list = self.format_string.split(" || ")
        db_handler = TsqlDBHandler(
            "dbo.T_LogEntries", "TestDB", "cfg.T_PythonLogs", fmt_list)
        formatter_cls = logging.Formatter(self.format_string)
        db_handler.setFormatter(formatter_cls)
        log_cls.addHandler(db_handler)
        log_cls.setLevel(level)
        log_cls.log(log_cls.level, self.start_msg)
        return log_cls


if __name__ == "__main__":
    logger = InitLogging(detail=True).create_file_log_class("DEBUG")
    logger.debug("debug msg")
    logger.info("info msg")
    logger.warning("warn msg")
