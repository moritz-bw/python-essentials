import os
import pickle
from loguru import logger


def create_format_string(format_sep: str, detail=False) -> str:
    """ Erstellt den Log-Format String
    Args:
        format_sep: str. Trenner fuer den Format-String
        detail: bool. Default `False` sets time, level, msg.
        Setting to `True` adds name, lineno, funcName, module, processName
    """
    format_items = ["{time:YYYY-MM-DD HH:mm:ss.SSS}", "{level}", "{message}"]
    if detail:
        format_items = [
            "{time:YYYY-MM-DD HH:mm:ss.SSS}", "{level: <8}", "{message}",
            "{extra[id]}",
            # process.id works also in non-parallel
            "{line}", "{function}", "{module}", "{process.name}",
            "{extra[run_id]}"
        ]
    fmt_string = f" {format_sep} ".join(format_items)
    return fmt_string

def get_run_id(id:str):
    pickle_path = os.path.join("logs","store_run_id.pkl")
    run_id_dict = {id: 1}
    # if pickle doesn't exist yet
    if os.path.exists(pickle_path):
        with open(pickle_path,"rb") as infile:
            run_id_dict.update(pickle.load(infile))
            if id in run_id_dict:
                run_id_dict[id]+=1
    with open(pickle_path, mode="wb") as outfile:
        pickle.dump(run_id_dict, outfile)
    return run_id_dict.get(id)



def get_logger(id: str, filepath:str , enqueue=False):
    """ logs to a file
    """
    fmt_string = create_format_string("||", detail=True)
    logger.remove()
    logger.add(filepath, format=fmt_string, enqueue=enqueue)  #,level="DEBUG")
    run_id = get_run_id(id)
    new_logger = logger.bind(id=id, run_id=run_id)
    return new_logger


if __name__ == "__main__":
    path_log = os.path.join("logs", "loguru_file.log")
    logger = get_logger("blub", path_log)
    logger.debug("debug msg")
    logger.info("info msg")
    logger.warning("warn msg")
