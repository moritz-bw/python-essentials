# Kontext

Folgende Data Engineering Situation:
* Quartalsweise mehrere csv-Dateien. Dateigrößen von 100 GB und mehr
* Data Engineering wurde sehr RAM-intensiv betrieben d.h. es wurde meist alles in den Arbeitsspeicher geladen
* Infrastruktur Situation ist katastrophal: 1 Linux Server, 32 GB RAM, 8 CPU

Es gab zwei Kommandozeilen-Tools welche wir relativ schnell eingeführt haben:
* [mongoimport](https://www.mongodb.com/try/download/database-tools)
* [sed](https://linux.die.net/man/1/sed)

Später kamen noch mehr Kommandozeilen-Tools dazu. 

# Fragen

1. Wie arbeite ich mit Linux-Tools auf Windows?
2. Wie gehe ich mit dem Output dieser Tools um?

Anhand zweier Demos zeige ich euch wie man das löst.

[next](script_os.py)

