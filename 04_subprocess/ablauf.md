
# Demo 1: Schritte

1. Befehl mongoimport ohne prefix verwenden
2. "dir" statt "mongoimport" verwenden
3. mongoimport in der Windows-Kommandozeile verwenden
4. Richtigen Befehl einfügen ('"C:\\Program Files\\MongoDB\\Tools\\100\\bin\\mongoimport"')
5. Andere Befehle zeigen "C:\Program Files\Git\usr\bin"
6. Korrekten Befehl testen
7. platform-modul mitnehmen (`platform.system()=="Linux"`)

# Learnings

zurück auf den Szenario-Slide und erweitern. Punkt 1 und 2 haben wir erfüllt. Punkt 3 nicht

* alles ist 1 außer der 0: 
  * 1: stderr
  * 0: stdout

Mit der aktuellen Lösung sind wir nicht in der Lage Fehlermeldungen zu verarbeiten und auszugeben. Als manuelles Skript funktioniert das, wenn ich aber mehrere Komponenten mit Python verknüpfen will dann wird das zum Problem. Ganz abgesehen davon wir mit `os.system` den Fehler nicht in eine Log-Tabelle oder Log-Datei bekommen.

# Demo 2: Anpassung auf subprocess

1. Hinzufügen der PIPE-commands: `, stdout=subprocess.PIPE, stderr=subprocess.PIPE)`
2. Verbessern des Fehlers: `and "Failed:" in stderr:`
3. An sich sagt die Dokumentation von subprocess dass man das `run`-command nutzen soll. Hier ein Beispiel wie das aussieht. 