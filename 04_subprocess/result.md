# Mögliche Erweiterungen

* An sich nutzt man nicht `print` sondern einen `logger`. Am besten [gehen die Warnungen in eine Datenbank-Tabelle und bei Fehler wird eine Mail versandt](../01_Logging/readme.md)
* Man kann mit dem Python-Modul `argparse` sehr schöne [Kommandozeilen-Tools erschaffen](https://docs.python.org/3/library/argparse.html)
* Man kann auch mit einem Python Modul namens [`pexpect`](https://pexpect.readthedocs.io/en/stable/) Sachen aus der Python Rückgabe auffangen
* subprocess.Popen ist analog zur implementation in Python-Modul [`asyncio`](https://docs.python.org/3/library/asyncio-subprocess.html). Man könnte jetzt diesen Code-Schnipsel erweitern und mit asyncio eine Workflow-Engine bauen um mehrere Datenverarbeitungsprozesse "gleichzeitig" laufen zu lassen. 