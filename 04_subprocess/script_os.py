""" Erstellt den CLI String
"""
import os
import platform

def cmd_string()->str:
    """ Erstellt CLI-Zeile
    """
    # cmd_str = '"C:\\Users\\batenm\\Documents\\mongodb-database-tools-windows-x86_64-100.6.1\\bin\\mongoimport" '
    cmd_str = "mongoimport "
    # if platform.system()=="Linux":
    #     cmd_str = "mongoimport "
    cmd_str += "--db=MovieLensSmall --collection=movies "
    cmd_str += "--type=csv --headerline "
    cmd_str += "--file=..\MovieLensSmal\Movies.csv"
    return cmd_str

def run():
    """ Ruft Skript aus
    """
    cmd = cmd_string()
    os_result = os.system(cmd)
    print("return code: ", os_result)


if __name__ == "__main__":
    run()
