""" Erstellt den CLI String
"""
import subprocess
import platform

def cmd_string()->str:
    """ Erstellt CLI-Zeile
    """
    cmd_str = '"C:\\Program Files\\MongoDB\\Tools\\100\\bin\\mongoimport" '
    if platform.system()=="Linux":
        cmd_str = "mongoimport "
    cmd_str += "--db=MovieLensSmall --collection=movies "
    cmd_str += "--type=csv --headerline "
    cmd_str += "--file=..\MovieLensSmal\Movies.csv"
    return cmd_str

def run():
    """ Ruft Skript aus
    """
    cmd = cmd_string()
    proc = subprocess.Popen(cmd, encoding="utf-8")#, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    if stderr: #and "Failed: " in stderr:
        raise RuntimeError(stderr)
    print("stdout: ", stdout)
    print("stderr: ", stderr)


if __name__ == "__main__":
    run()
