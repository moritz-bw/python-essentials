""" Erstellt den CLI String
"""
import subprocess
import platform

def cmd_string()->str:
    """ Erstellt CLI-Zeile
    """
    cmd_str = '"C:\\Program Files\\MongoDB\\Tools\\100\\bin\\mongoimport" '
    if platform.system()=="Linux":
        cmd_str = "mongoimport "
    cmd_str += "--db=MovieLensSmall --collection=movies "
    cmd_str += "--type=csv --headerline "
    cmd_str += "--file=..\MovieLensSmal\Movies.csv"
    return cmd_str

def run():
    """ Ruft Skript aus
    """
    cmd = cmd_string()
    try:
        proc = subprocess.run(cmd, encoding="utf-8", check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as error:
        raise Exception(error.stderr).with_traceback(error.__traceback__) from error
    print("stdout: ", proc.stdout)


if __name__ == "__main__":
    run()
