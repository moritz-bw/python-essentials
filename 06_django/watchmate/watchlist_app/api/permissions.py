from rest_framework import permissions

class AdminOrReadOnly(permissions.IsAdminUser):
    """ Allows only admins to alter data
    """ 
    def has_permission(self, request, view):
        admin_permission = bool(request.user and request.user.is_staff)
        return request.method == "GET" or admin_permission

class ReviewUserOrReadOnly(permissions.BasePermission):
    """ Users can only change their own review
    """
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.review_user == request.user
